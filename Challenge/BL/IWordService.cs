﻿namespace BL;

public interface IWordService
{
    public List<String> GetWords(string path);
}