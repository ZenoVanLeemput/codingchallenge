﻿using System.Text;
using DAL;

namespace BL;

public class WordService : IWordService
{
    private readonly int CHARSPERWORD = 6;

    private IWordRepository _repo;

    public WordService(IWordRepository repo)
    {
        _repo = repo;
    }

    public List<String> GetWords(string path)
    {
        var chars = _repo.ReadWords(path);
        string words = MergeChars(chars);
        return SplitWords(chars, words);
    }

    private String MergeChars(List<String> chars)
    {
        StringBuilder mergedChars = new StringBuilder();
        
        foreach (var wordPart in chars)
        {
            mergedChars.Append(wordPart);
        }

        return mergedChars.ToString();
    }

    private List<String> SplitWords(List<String> chars, string words)
    {
        List<String> wordList = new List<String>();

        for (int i = 0; i < words.Length; i=i+CHARSPERWORD)
        {
            string word = words.Substring(i, CHARSPERWORD);

            if (chars.Contains(word)&&!wordList.Contains(word))
            {
                wordList.Add(word);
            }
        }

        return wordList;
    }
}

