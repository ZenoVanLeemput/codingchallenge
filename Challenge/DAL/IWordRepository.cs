﻿namespace DAL;

public interface IWordRepository
{
    public List<String> ReadWords(string path);
}

