﻿namespace DAL;

public class WordRepository : IWordRepository
{
    public List<String> ReadWords(string path)
    {
        try
        {
            return File.ReadAllLines(path).ToList();
        }
        catch (IOException e)
        {
            throw new IOException("Bestand niet gevonden, controleer of bestand bestaat in pad: " + path);
        }
    }
}

