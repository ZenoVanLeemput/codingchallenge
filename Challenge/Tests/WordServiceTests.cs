﻿using BL;
using DAL;

namespace Tests;

public class WordServiceTests
{
    private IWordService _service;

    [SetUp]
    public void Setup()
    {
        _service = new WordService(new WordRepository());
    }

    [Test]
    public void TestGetWords()
    {
        string path = @"../../../../../inputTest.txt";
        var words = _service.GetWords(path);
        Assert.AreEqual(3, words.Count);
        Assert.Contains("feeder",words);
        Assert.Contains("peeved", words);
        Assert.Contains("citril", words);
    }
}

