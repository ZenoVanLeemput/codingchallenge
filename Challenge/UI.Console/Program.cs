﻿using BL;
using DAL;

IWordService wordService = new WordService(new WordRepository());

string path = @"../../../../../input.txt";
List<String> words = new List<string>();

try
{
    words = wordService.GetWords(path);
}
catch (IOException e)
{
    Console.WriteLine(e.Message);
    return;
}

Console.WriteLine("Printing all words: ");

foreach (var word in words)
{
    Console.WriteLine(word);
}

